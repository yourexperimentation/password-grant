package experimentation.oauth2framework.service;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * Common function using in the project.
 */
public interface Utils {

    /**
     * Create String Collection from
     * Object
     *
     * @param myCollection
     *               Collection to convert
     * @param <T>
     *          Generic Type
     *
     * @return A String collection from generic type
     */
    <T> Collection<String> convertToString(Collection<T> myCollection);

    /**
     * Create SimpleGrantedAuthority
     * From User role collection
     *
     * @param myAuthroties
     *                  User authorities collection
     * @param <T>
     *           Generic type
     *
     * @return A simpleGrandAuthority Collection
     */
    <T> Collection<GrantedAuthority> getAuthorities(Collection<T> myAuthroties);
}
