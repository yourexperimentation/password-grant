package experimentation.oauth2framework.service.impl;

import experimentation.oauth2framework.service.Utils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

@Service
public class UtilsImpl implements Utils {

    @Override
    public <T> Collection<String> convertToString(Collection<T> myCollection){
        Collection<String> stringCollection = new HashSet<>();
        myCollection.forEach( t -> stringCollection.add(t.toString()));
        return stringCollection;
    }

    public  <T> Collection<GrantedAuthority> getAuthorities(Collection<T> myAuthroties) {
        Collection<GrantedAuthority> roles = new ArrayList<>();
        myAuthroties.forEach(r -> roles.add( new SimpleGrantedAuthority("ROLE _" + r.toString())) );
        return roles;
    }
}
