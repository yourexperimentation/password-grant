package experimentation.oauth2framework.service.impl;

import experimentation.oauth2framework.model.MyUser;
import experimentation.oauth2framework.repositories.UserRepository;
import experimentation.oauth2framework.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Create spring security Pincipal User
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    private final Utils utils;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, Utils utils) {
        this.userRepository = userRepository;
        this.utils = utils;
    }

    /**
     *
     * @param userId
     *             The user's id
     * @return
     *        UserDetails for Spring Security (Principal)
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        Optional<MyUser> myUserOptional = this.userRepository.findByUsername(userId);
        return myUserOptional.map(this::buildMyUserDetails).orElseThrow(() ->new UsernameNotFoundException(" Username " + userId + " Not Found" ));
    }

    /**
     *
     * @param myUser
     *             User from DataBase
     * @return  UserDetails for Spring Security (Principal)
     */
    private UserDetails buildMyUserDetails(MyUser myUser){

      return   User.builder()
                .username(myUser.getUsername())
                .password(myUser.getPassword())
                .accountExpired(false)
                .accountLocked(false)
                .authorities(utils.getAuthorities(myUser.getRoles()))
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }

    @Bean
    public PasswordEncoder bCryptPasswordEncoder (){
        return new BCryptPasswordEncoder();
    }
}
