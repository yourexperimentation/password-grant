package experimentation.oauth2framework.service.impl;

import experimentation.oauth2framework.model.Client;
import experimentation.oauth2framework.repositories.ClientRepository;
import experimentation.oauth2framework.service.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

/**
 * Create the BaseClient Credentials.
 */
@Service
public class ClientDetailsServiceImpl implements ClientDetailsService {

    private final ClientRepository clientRepository;
    private final Utils utils;
    // the access token validity second
    private static final int ACCES_TOKEN_VALIDITY_SECONDS = 900;

    @Autowired
    public ClientDetailsServiceImpl(ClientRepository clientRepository, Utils utils) {
        this.clientRepository = clientRepository;
        this.utils = utils;
    }

    /**
     *
     * @param clientId
     *               the oauth2 client id
     * @return Client Details for Oauth2
     *
     * @throws ClientRegistrationException
     */
    @Override
    @Transactional
    public ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
        Optional<Client> optionalClient = this.clientRepository.findByClientId(clientId);
        return optionalClient.map(this::baseClientDetailsBuilder).orElseThrow(() -> new ClientRegistrationException("Error while loading " + clientId));
    }

    /**
     *
     * @param client
     *             the client from DataBase
     * @return Oauth2 Based Client
     */

    private BaseClientDetails baseClientDetailsBuilder(Client client) {
        BaseClientDetails baseClientDetails = new BaseClientDetails();
        baseClientDetails.setClientId(client.getClientId());
        baseClientDetails.setClientSecret(client.getClientSecret());
        baseClientDetails.setScope(utils.convertToString(client.getScopes()));
        baseClientDetails.setAuthorities(utils.getAuthorities(client.getRoles()));
        baseClientDetails.setAuthorizedGrantTypes(client.getGrandTypes());
        baseClientDetails.setAccessTokenValiditySeconds(ACCES_TOKEN_VALIDITY_SECONDS);
        return baseClientDetails;
    }

}
