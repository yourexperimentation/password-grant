package experimentation.oauth2framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Oauth2FrameworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(Oauth2FrameworkApplication.class, args);
    }

}
