package experimentation.oauth2framework.repositories;

import experimentation.oauth2framework.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    /**
     *
     * @param client_id
     *               the client Id to retrieve
     * @return an optional containing the client with id given as parameter
     */
    Optional<Client> findByClientId(String client_id);
}
