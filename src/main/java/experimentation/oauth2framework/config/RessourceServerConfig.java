package experimentation.oauth2framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * Ressource Server Configuration
 */
@Configuration
@EnableResourceServer
public class RessourceServerConfig extends ResourceServerConfigurerAdapter {

    private final TokenStore tokenStore;

    @Autowired
    public RessourceServerConfig(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }
    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
        resources.tokenStore(tokenStore);

    }
    /**
     * Give all access to clients & users
     * url in order to create them.
     *
     * @param http
     *            http Security Configurer
     * @throws Exception
     */
    public void configure(HttpSecurity http) throws Exception {
       http.csrf().disable()
               .authorizeRequests()
               .antMatchers("/clients").permitAll()
               .antMatchers("/users").permitAll()
               .anyRequest().authenticated()
               .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
