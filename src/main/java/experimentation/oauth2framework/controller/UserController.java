package experimentation.oauth2framework.controller;

import experimentation.oauth2framework.model.MyUser;
import experimentation.oauth2framework.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    @Autowired
    public UserController(UserRepository userRepository,
                          PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping
    public ResponseEntity<MyUser> createUser(@RequestBody MyUser myUser){
        myUser.setPassword(passwordEncoder.encode(myUser.getPassword()));
        MyUser myUser1 = this.userRepository.save(myUser);
        return new ResponseEntity<>(myUser1, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Collection<MyUser>> getAllUser() {
        Collection<MyUser> collection = this.userRepository.findAll();
        return new ResponseEntity<>(collection, HttpStatus.OK);
    }
}
