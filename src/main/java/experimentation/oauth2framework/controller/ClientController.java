package experimentation.oauth2framework.controller;

import experimentation.oauth2framework.model.Client;
import experimentation.oauth2framework.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/clients")
public class ClientController {
    private final ClientRepository clientRepository;
    private final PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public ClientController(ClientRepository clientRepository,
                            PasswordEncoder bCryptPasswordEncoder) {
        this.clientRepository = clientRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping
    public ResponseEntity<Client> createClient(@RequestBody Client client){
      client.setClientSecret(bCryptPasswordEncoder.encode(client.getClientSecret()));
      Client client1 = clientRepository.save(client);
      return new ResponseEntity<>(client1, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Collection<Client>> getClient() {
        Collection<Client> clients = this.clientRepository.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
}
