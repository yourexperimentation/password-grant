package experimentation.oauth2framework.model.privilege;

/**
 *
 */
public enum Scope {
    READ,
    WRITE,
    TRUST
}
