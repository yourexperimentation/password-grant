package experimentation.oauth2framework.model.privilege;

/**
 *
 */
public enum Role {
    USER,
    ADMIN
}
