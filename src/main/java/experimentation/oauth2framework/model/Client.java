package experimentation.oauth2framework.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import experimentation.oauth2framework.model.privilege.Role;
import experimentation.oauth2framework.model.privilege.Scope;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String clientId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String clientSecret;

    @Enumerated(value = EnumType.STRING)
    @ElementCollection(targetClass = Role.class)
    @LazyCollection(LazyCollectionOption.FALSE)
    Collection<Role> roles = new HashSet<>();

    @ElementCollection(targetClass = Scope.class)
    @Enumerated(value = EnumType.STRING)
    @LazyCollection(LazyCollectionOption.FALSE)
    Collection<Scope> scopes = new HashSet<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    Collection<String> grandTypes = new HashSet<>();
}
