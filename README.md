# Oauth2 Password Grant 

Pour realiser ce projet, je me suis referé au developpeur Guide de spring (https://github.com/spring-projects/spring-security-oauth/blob/master/docs/oauth2.md)

Pour comprendre le grant implementé dans ce projet, veillez voir (https://www.oauth.com/oauth2-servers/access-tokens/password-grant/)
Pour tout savoir le protocole oauth2 (https://tools.ietf.org/html/rfc6749#section-3.3)

# Tester le projet

Afin de tester ce projet, veillez suivre les étapes suivantes

1. faites un tour dans le package model afin de comprendre la dataModel
2. Creer un client à travers l'url (localhost:9090/clients)
3. Creer un ressourceOwner à travers l'url (localhost:9090/users)
4. Creer un token à travers l'url (localhost:9090/oauth/token)
5. Acceder ensuite à la ressource protegée à travers l'url (localhost:9090/secure) en passant l'acces token

